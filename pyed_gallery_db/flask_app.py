from flask import Flask, render_template, request, url_for, redirect, g
import flask
from image_helper import ImageHelper
from gallery_sqlite import load_db

app = Flask(__name__)
app.config.from_json('config.json')

def get_db():
    db = getattr(g, 'db', None)
    if db is None:
        db = g.db = load_db(app.config['DB_FILE'])
    return db

def get_im_service():
    s = getattr(g, 'image_service', None)
    if s is None:
        s = g.s = ImageHelper(app.config['IMAGE_REPOSITORY'])
    return s

@app.route('/static/<path:path>')
def send_statics(path):
    return url_for('static', filename=path)


@app.route('/')
def index():
    db = get_db()
    galleries = db.get_all_galleries()

    first_gal = galleries[0]
    gallery_children = db.get_gallery(first_gal['name'])

    return render_template('gallery-view.html',
                           galleries_list=galleries,
                           gallery_name=first_gal['name'],
                           children=gallery_children)


@app.route("/upload_page")
def upload_page():
    return render_template("upload-content.html")


@app.route("/upload", methods=['POST'])
def upload():
    galleryname = request.form['gallery_name']
    uploaded_files = request.files.getlist('myfile[]')

    im_service = get_im_service()
    db = get_db()

    new_paths = []
    for img in uploaded_files:
        new_paths.append(im_service.image_content_generator(img))

    db.create_gallery(galleryname, new_paths)

    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run('0.0.0.0', debug = True)
