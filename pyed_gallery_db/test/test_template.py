from jinja2 import Environment, PackageLoader, select_autoescape

env = Environment(
    loader=PackageLoader('pyed_gallery_db', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)

template = env.get_template('gallery-view.html')

data = {
  'galleries_list': [
      {'name': 'gallery 1'},
      {'name': 'gallery 2'}
  ],
  'gallery': {
      'name': 'abc',
      'children': [
          {
              'name': 'Wonka 1',
              'url': 'http://files2.ostagram.ru/uploads/style/image/89773/thumb_img_0107385e40.jpg'
          },
          {
              'name': 'Wonka 2',
              'url': 'http://files2.ostagram.ru/uploads/style/image/89773/thumb_img_0107385e40.jpg'
          }
      ]
  },
}
galleries_list = data['galleries_list']
gallery_name = data['gallery']['name']
gallery_children = data['gallery']['children']

print template.render(
    galleries_list=galleries_list,
    gallery_name=gallery_name,
    children=gallery_children
)

